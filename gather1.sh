set -x

###
### PRE-RUN
###

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

__basedir1="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
__var0="$0" && __var0="${__var0##*/}"

source ${__basedir1}/.lib0.sh

check_if_already_running1

determine_max_tasks1

###
### USERSPACE CONFIGURATION
###

load_configuration_file1 ${__var0}.example
load_configuration_file1

load_configuration_file1 scan2.sh

###
### FUNCTIONS
###

load_library_file1
load_library_file1 lib1.sh

###
### LOOPS
###

do_cleanup1

for _host1 in `find ${__ready_root_dir1} -name snmp_scan_results1* -mtime -7 | sort -V`
do
	source ${_host1}
	derive_counter1
	if [ "${sysname1}" = "" ]; then sysname1="unnamed_device${_counter1}"; fi
	derive_sysname2

	if [ -f ${__build_dir1}/${sysname2}.cfg ]; then continue; fi

	derive_nagios_snmp_access_string1
	identify_device1

	generate_host1

	_base_rta1="500"
	_system_reachability_rrd_file1="${pnp4nagios_rrd_directory1:-/var/lib/pnp4nagios}/${sysname2}/System_Reachability_rta.rrd"
	if [ -f ${_system_reachability_rrd_file1} ]
	then
	 _base_rta1="`rrdtool graph dummy -s '-1m' ${rrdtool_rrdcached1} DEF:rta_avg1=\""${_system_reachability_rrd_file1}"\":1:AVERAGE VDEF:rta_avg2=rta_avg1,95,PERCENT PRINT:rta_avg2:%.0lf | grep -v '0x0'`"
	 if [[ ! ${_base_rta1} =~ ^[0-9]+$ ]]; then _base_rta1="500"; fi
	fi
	generate_host_icmp_check1

	generate_host_sysdescr_check1

	###
	# if [ `find ${__ready_root_dir1} -name snmp_mib_scan_results1___${_counter1} | wc -l` -eq 1 ]; then source <(cat `find ${__ready_root_dir1} -name snmp_mib_scan_results1___${_counter1}`); else continue; fi
	if [ `find ${__ready_root_dir1} -name snmp_mib_scan_results1___${_counter1} | wc -l` -eq 1 ]; then source `find ${__ready_root_dir1} -name snmp_mib_scan_results1___${_counter1}`; else continue; fi
	###

	if [ "${snmpEngineTime[0]}" != "" ] && [ "${snmpEngineTime[0]/ seconds/}" != "0" ]
	then
		generate_host_uptime_check1

	elif [ "${hrSystemUptime[0]}" != "" ]
	then
		generate_host_uptime_check2
	else
		generate_host_uptime_check3
	fi

	###
	###	
	###

	for _index1 in ${!ifName[*]}
	do
	 if [[ "${ifAlias[${_index1}]}" =~ ${match_interesting_ifalias1} ]] && [[ ! "${ifAlias[${_index1}]}" =~ ${except_interesting_ifalias1} ]]
	 then

	  if [ "${disregard_ifadmindown1}" = "" ]
	  then
	   if [ "${ifAdminStatus[${_index1}]}" = "" ]; then continue; fi
	  else
	   if [ "${ifAdminStatus[${_index1}]}" = "down" ] || [ "${ifAdminStatus[${_index1}]}" = "" ]; then continue; fi
	  fi

	  if [[ "${ifHighSpeed[${_index1}]}" =~ (^0$|^$) ]]
	  then
	   generate_interface_traffic2

	  elif [[ "${ifDescr[${_index1}]}" =~ ^(vfc) ]] && [[ ${sysdescr1} =~ [Cc]isco ]] || [[ "${ifAlias[${_index1}]}" =~ ^(I1:) ]]
	  then
	   generate_interface_traffic2
	  else
	   generate_interface_traffic1
	  fi

	  if [ "${no_ifoperstatus1}" = "" ]; then generate_interface_ifoperstatus1; fi

	  if [[ ${ifType[${_index1}]} =~ (ethernetCsmacd|ieee80211) ]]
	  then
	   generate_interface_ifinerrors1
	   generate_interface_ifinbroadcastpkts1
	  fi
	 fi
	done

	###
	###
	###

	for _index1 in ${!bgpPeerIdentifier[*]}; do generate_bgp_session_status1; done

	###
	###
	###

	if [ ${#cEigrpPeerAddr[*]} -gt 0 ]
	then
	 if [ "`grep -sc 'bkos_check_snmp_cisco_eigrp_neighbour_count.sh' ${__ready_dir1}/${sysname2}.cfg`" = "1" ]
	 then
	  _previous_expected_neighbour_count1=`grep 'bkos_check_snmp_cisco_eigrp_neighbour_count.sh' ${__ready_dir1}/${sysname2}.cfg | sed 's/.* //'`
	 else
	  _previous_expected_neighbour_count1=0
	 fi

	 if [ ${#cEigrpPeerAddr[*]} -gt ${_previous_expected_neighbour_count1} ]; then _expected_neighbour_count1=${#cEigrpPeerAddr[*]}; else _expected_neighbour_count1=${_previous_expected_neighbour_count1}; fi
	 generate_eigrp_neighbour_count1
	fi

	###
	###
	###

	for _index1 in ${!mtxrQueueTreeName[*]}
	do
	 generate_mikrotik_routeros_queue_tree_fwd_bps1
	 generate_mikrotik_routeros_queue_tree_dropped_pps1
	 generate_mikrotik_routeros_queue_tree_pcq_queues1
	done

	for _index1 in ${!mtxrWlApSsid[*]}
	do
	 if [ "${mtxrWlApBssid[${_index1}]}" = "" ]
	 then
	  if [ ${mtxrWlApFreq[${_index1}]} != "0" ]; then generate_mikrotik_routeros_wlan_physical_ap1
	  else generate_mikrotik_routeros_wlan_virtual_ap1; fi
	 fi
	done

	###
	###
	###

	if [[ ${_hostgroups1} =~ cisco-waas ]]; then generate_cisco_waas_cwoTfoStatsLoadStatus1; fi

	for _index1 in ${!cwoAoStatsBwOpt[*]}
	do
	 _waas_module_chars1=(`echo ${_index1} | sed -r -e 's/(.)/\1 /g'`)
	 _module_name_char_count1=${#_waas_module_chars1[*]}

	 for _index2 in ${_waas_module_chars1[*]}; do _waas_module_oid1="${_waas_module_oid1}.`printf '%d' "'${_index2}"`"; done

	 generate_cisco_waas_cwoAoStatsBwOpt1
	 unset _waas_module_oid1
	done

	###
	###
	###

	if [ ${#clsSysApConnectCount[*]} -gt 0 ]; then generate_cisco_wlc_lap_clsSysApConnectCount1; fi

	###

	_ap_ctr1=1
	for _index1 in ${!cLApName[*]}
	do
	 ifs_set_newline2
	 for _index2 in ${_index1}; do _ap_index1="${_ap_index1}.`printf %d 0x${_index2}`"; done
	 ifs_set_default1

	 generate_cisco_wlc_lap_presence1

	 unset _ap_index1
	let _ap_ctr1++
	done
	unset _ap_ctr1

	###

	for _index1 in ${!cbQosCMPrePolicyPkt64[*]}
	do
	 if [[ ${sysname1} =~ [0-9](CS|WS|WR|DS) ]] || [[ ${sysname1} =~ GBLDS2LSF ]]
	 then

	  _ifalias1="${ifAlias[${cbQosIfIndex[${_index1%%.*}]}]}"
	  if [[ "${_ifalias1}" =~ ${match_interesting_ifalias1} ]] && [[ ! "${_ifalias1}" =~ ${except_interesting_ifalias1} ]]
	  then
	   _policy_map1="${cbQosPolicyMapName[${cbQosConfigIndex[${_index1%%.*}.${cbQosParentObjectsIndex[${_index1}]}]}]}"
	   _class_map1="${cbQosCMName[${cbQosConfigIndex[${_index1}]}]}"

	   if [[ ! ${_class_map1} =~ (class-default|SCAVENGER|BULK) ]]
	   then
	    _alarm_threshold_bps1="-c ,,:0"
	    _alarm_threshold_pps1="-c ,:0"
	   fi

	   if [ "${_policy_map1}" != "" ] && [ "${_class_map1}" != "" ]
	   then
	    generate_cisco_cbqos_bps1
	    generate_cisco_cbqos_pps1
	   fi

	  unset _alarm_threshold_pps1 _alarm_threshold_bps1

	  fi

	 fi
	done

	###
	for _index1 in ${!rttMonEchoAdminTargetAddress[*]}
	do
	 ifs_set_newline1
	 for _index2 in ${rttMonEchoAdminTargetAddress[${_index1}]}
	 do
	  ifs_set_default1
	  for _index3 in ${_index2[*]}
	  do
	   _ipsla_monitored_device_ip1="${_ipsla_monitored_device_ip1}.`printf '%d' "0x${_index3}"`"
	  done

	  generate_cisco_ipsla_sensor1

	  unset _ipsla_monitored_device_ip1
	 done
	done
	###
		 
	###
	unregister_variables1
	###
done
wait

cp ${__build_dir1}/* ${__ready_dir1}/ 2> /dev/null

# EOF
