#!/bin/bash
#set -x

###
### PRE-RUN
###

declare -A _dynamic_map

###
### FUNCTIONS
###

gug_chart_dynamic_network_map()
{
sed -i "/address .*/a\ parents ${_host_map[*]%,*}" ${_ready_dir}/${__host}.cfg
}

gug_chart_dynamic_network_map_reverse()
{
_host_reverse="`echo ${_host_reverse} | sed -e 's/^Ep_/E_/' -e 's/^Ip_//'`"

sed -i "/address .*/a\ parents ${_host_map[*]%,*}" ${_ready_dir}/${_host_reverse}.inwep.wan.cfg
}

###
### LOOPS
###

for __host in `echo ${hosts_list[*]} | grep -Ev "^E-"`
do
	if [ -f ${_build_dir}/${__host}___map_data_stage1 ] && [ -f ${_ready_dir}/${__host}.cfg ]
	then
		sort -ui ${_build_dir}/${__host}___map_data_stage1 > ${_build_dir}/${__host}___map_data

		for _map_host_parent in `grep -E "^(I|E)c_" ${_build_dir}/${__host}___map_data | sed -r -e 's/^Ic_//' -e 's/^Ec_/E_/'`
		do
			_host_map="${_host_map%%.*}${_map_host_parent},"
		done
		if [ "${_host_map[*]}" != "" ]
		then
			gug_chart_dynamic_network_map
		fi
		_host_map=""
	fi
done

for _host_reverse in `grep -E "^(I|E)p_" ${_build_dir}/*___map_data | sed -r -e 's/^.*://'`
do
	for _host_map_child in `grep -E "^${_host_reverse}" ${_build_dir}/*___map_data | sed -r -e "s,${_build_dir}/,," -e 's/\..*//'`
	do
		_host_map="${_host_map}${_host_map_child},"
	done
	if [ "${_host_map[*]}" != "" ]
	then
		gug_chart_dynamic_network_map_reverse
	fi
	_host_map=""
done




#echo ${_host_map_reversed[*]}

# rm ${_build_dir}/*___map_data

# ifs_set_newline
# for __host in `grep -E "^(I|E)p_" ${_build_dir}/*___map_data_stage1 | sed -e "s,${_build_dir}/,," -e 's/___map_data_stage1:/ /'`
# do
	# if [ -f ${_ready_dir}/${__host[*]%% *}.cfg ]
	# then
		# echo ${__host[*]##* } >> ${_build_dir}/${__host[*]%% *}___map_data_stage2
	# fi
# done
# ifs_set_default

# for __host in `ls ${_build_dir}/*___map_data_stage2`
# do
	# sort -ui ${__host} > ${__host%___map_data_stage2}___map_data

	# for _map_host_parent in `grep -E "^(I|E)p_" ${__host%___map_data_stage2}___map_data | sed -r -e 's/^Ip_//' -e 's/^Ep_/E_/'`
	# do
		# __host_map="${__host_map%%.*}${_map_host_parent},"
	# done
	# if [ "${__host_map[*]}" != "" ]
	# then
		# gug_chart_dynamic_network_map
	# fi
	# __host_map=""
# done



###
### POST-RUN
###

#_script_run_time=$(( `printf '%(%s)T\n' -1` - ${_script_start_time} ))

# EOF
