set -x

###
### PRE-RUN
###

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

__basedir1="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
__var0="$0" && __var0="${__var0##*/}"

source ${__basedir1}/.lib0.sh

check_if_already_running1

determine_max_tasks1

load_configuration_file1

###
### LIBRARIES
###

load_library_file1
load_library_file1 lib1.sh

###
### LOOPS
###

do_cleanup1
do_cleanup2

for _host1 in `find ${__ready_root_dir1} -name snmp_scan_results1*`
do
	do_multitasking1

	source ${_host1} && \
	\
	if [ `ping -q -c 2 -A -W 10 ${ipv41} > /dev/null 2>&1; echo $?` -eq 0 ]
	then
		derive_counter1
		walk_or_bulkwalk1

		for _oid1 in ${oids1[*]}
		do
			gather_snmp_data1
			if [ ${_snmpwalk_exit_code1} -ne 0 ]
			then
				# If snmpwalk returns an error, remove
				# the device data file altogether, thereby
				# preventing the Gatherer script from sourcing
				# incomplete information for sensors building
				rm ${_snmp_mib_scan_results1}___${_counter1}
				break
			fi
		done
		# In an event of snmpwalk error, proceed
		# immediately to the next device in list
		if [ ${_snmpwalk_exit_code1} -ne 0 ]; then continue; fi		
	fi &
done
wait

cp ${__build_dir1}/* ${__ready_dir1}/ 2> /dev/null

# EOF
