###
###
###

###
### GLOBAL VARIABLES
###

__build_root_dir1="${__basedir1}/.build"
__ready_root_dir1="${__basedir1}/.ready"
__config_root_dir1="${__basedir1}/.config"
__lib_root_dir1="${__basedir1}/.lib"
__redoxed_root_dir1="${__basedir1}/.redoxed"
__archive_root_dir1="${__basedir1}/.archive"

__build_dir1="${__build_root_dir1}/${__var0}"
__ready_dir1="${__ready_root_dir1}/${__var0}"
__archive_dir1="${__archive_root_dir1}/${__var0}"

__config_file1="${__config_root_dir1}/${__var0}"
__lib_file1="${__lib_root_dir1}/${__var0}"

###
### CORE FUNCTIONS
###

check_if_already_running1()
{
if [ `pgrep -f ${__basedir1}/${__var0} | wc -w` -gt 3 ]; then exit 1; fi
}

determine_max_tasks1()
{
__max_tasks1=$(( `grep -c ^processor /proc/cpuinfo` * 32 ))
}

do_cleanup1()
{
mkdir -p ${__build_dir1}
rm -f ${__build_dir1}/* 2> /dev/null

mkdir -p ${__ready_dir1}
}
do_cleanup2()
{
mkdir -p ${__ready_dir1}
rm -f ${__ready_dir1}/* 2> /dev/null

mkdir -p ${__ready_dir1}
}

do_multitasking1()
{
while [ `jobs | wc -l` -gt ${__max_tasks1:-16} ]; do sleep 1; done
}

load_configuration_file1()
{
if [ "$1" != "" ]
then
        if [ -f ${__config_root_dir1}/$1 ]; then source ${__config_root_dir1}/$1; fi
else
        if [ -f ${__config_file1} ]; then source ${__config_file1}; fi
fi
}

load_library_file1()
{
if [ "$1" != "" ]
then
        if [ -f ${__lib_root_dir1}/$1 ]; then source ${__lib_root_dir1}/$1; fi
else
        if [ -f ${__lib_file1} ]; then source ${__lib_file1}; fi
fi
}

# EOF

