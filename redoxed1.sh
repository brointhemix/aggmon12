set -x

###
### PRE-RUN
###

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

__basedir1="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
__var0="$0" && __var0="${__var0##*/}"

source ${__basedir1}/.lib0.sh

check_if_already_running1

determine_max_tasks1

load_configuration_file1

###
### LIBRARIES
###

load_library_file1
load_library_file1 lib1.sh

###
### LOOPS
###

do_cleanup1

for _host1 in `find ${__ready_root_dir1} -name snmp_scan_results1*`
do
	do_multitasking1

	source ${_host1} && \
	derive_sysname2 && \
	_runtime_configuration_file1="${__build_dir1}/${sysname2}___runtime" && \
	if [ -f ${_runtime_configuration_file1} ]; then continue; fi && \
	\
	if [[ ${sysdescr1} =~ (IOS XR|IOS-XE|LINUX_IOSD| IOS |NX-OS) ]]
	then
		${__redoxed_root_dir1}/worker1.exp ${ipv41} ${login_username1} ${login_password1} | sed -r -e 's/\r+//' -e 's/[[:blank:]]*$//' > ${_runtime_configuration_file1}

		if [ `grep -Ec "^### ERROR: " ${_runtime_configuration_file1}` -ne 0 ]
		then
			rm ${_runtime_configuration_file1}
		fi
	fi &
done
wait

cp ${__build_dir1}/* ${__ready_dir1}/ 2> /dev/null

# EOF
