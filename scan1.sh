set -x

###
### PRE-RUN
###

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

__basedir1="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
__var0="$0" && __var0="${__var0##*/}"

source ${__basedir1}/.lib0.sh

check_if_already_running1

determine_max_tasks1

load_configuration_file1

###
### LIBRARIES
###

load_library_file1

###
### LOOPS
###

do_cleanup1

_counter1=1
for _network1 in ${ipv4_networks1[*]}
do
	do_multitasking1

	discover_icmp1 &

	let _counter1++
done
wait

sleep 1

_counter1=1
for _network1 in `ls ${__build_dir1}/icmp_scan_results1___* 2> /dev/null`
do
	for _host1 in `grep -Eiv '( : - - -|Error)' ${_network1} | cut -d ' ' -f 1`
	do
		do_multitasking1

		pickup_snmp_credentials_if_available1
		for _snmp_access1 in "${snmp_access1[@]}"
		do
			if [ `ping -q -c 2 -A -W 5 ${_host1} > /dev/null 2>&1; echo $?` -eq 0 ]
			then
				discover_snmp1
				discover_snmp3
			fi
		done &

		let _counter1++
	done
done
wait

cp ${__build_dir1}/* ${__ready_dir1}/ 2> /dev/null

# EOF
