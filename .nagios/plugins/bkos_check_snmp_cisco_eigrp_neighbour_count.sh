# set -x

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

###
### INPUT
###

# SNMP access string
# Hostname or IP address
# Alert threshold 
arg1=("$@")

###
### FUNCTIONS
###

walk_or_bulkwalk1()
{
if [[ ${arg1[0]} =~ (-v1|-v 1) ]]; then _snmpwalk1="snmpwalk"; else _snmpwalk1="snmpbulkwalk"; fi
}

get_data1()
{
# cEigrpPeerAddr
_get_data1=(`${_snmpwalk1} -C cp -O qv ${arg1[0]} ${arg1[1]} .1.3.6.1.4.1.9.9.449.1.4.1.1.3`)
_retval1=$?
}

get_data2()
{
# cEigrpUpTime
_get_data2=(`${_snmpwalk1} -C cp -O qv ${arg1[0]} ${arg1[1]} .1.3.6.1.4.1.9.9.449.1.4.1.1.6`)
_retval2=$?
}

print_results1()
{
echo -e "EIGRP Neighbours Count: ${_get_data1[-1]}"

if [ ${arg1[2]} -gt ${_get_data1[-1]} ]
then
	exit 2
else
	exit 0
fi
}

print_error1()
{
echo "${get_data1[*]}"

exit 3
}

print_help1()
{
echo ""
echo "Usage:"
echo "$0 \"<SNMP Access String>\" <Hostname or IP address> <Expected EIGRP Neighbours Count>"
echo ""
}

###
### LOOPS
###

if [ "$2" = "" ]; then print_help1; exit 1; fi

_retval1=0
_retval2=0

walk_or_bulkwalk1
get_data1
# get_data2

if [ $(( ${_retval1} + ${_retval2} )) -eq 0 ] && [[ ! ${_get_data1[*]} =~ No ]]; then print_results1; else print_error1; fi

# EOF
