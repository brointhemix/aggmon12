###
### COMMON FUNCTIONS
###

derive_counter1() { _counter1=${_host1##*___}; }

derive_sysname2() { sysname2="${sysname1// /_}"; }

ifs_set_newline1() { _defaultIFS=${IFS} && IFS=$'\n'; }
ifs_set_newline2() { _defaultIFS=${IFS} && IFS=':'; }
ifs_set_default1() { IFS=${_defaultIFS}; }

unregister_variables1() { for _oid1 in ${oids1[*]}; do unset ${_oid1}; done; }

# EOF
