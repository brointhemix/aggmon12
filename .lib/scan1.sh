###
### lib scan1.sh
###

###
### GENERAL VARIABLES
###

_icmp_scan_results1="${__build_dir1}/icmp_scan_results1"
_snmp_scan_results1="${__build_dir1}/snmp_scan_results1"

###
### FUNCTIONS
###

discover_icmp1()
{
fping -q -C 3 -p 25 -r 1 -g ${_network1} >> ${_icmp_scan_results1}___${_counter1} 2>&1
}

pickup_snmp_credentials_if_available1()
{
_snmp_scan_results2="`grep -s "\<${_host1}\>" ${__ready_dir1}/${_snmp_scan_results1##*/}* | sed -r -e 's/(___[0-9]+):.*/\1/'`"
if [ "${_snmp_scan_results2}" != "" ]; then source ${_snmp_scan_results2}; snmp_access1[0]="${snmp1}"; else unset snmp_access1[0]; fi
}

discover_snmp1()
{
_sysdescr1="`snmpget -O qv ${_snmp_access1} ${_host1} sysDescr.0 2> /dev/null | sed -E 's/[[:space:]]+$/ /' | sed -e 's/$/\n /' | tr -d '\r\n' | sed -E 's/[[:space:]]+$//' | grep -Eiv "(No Such|End of)"`"
_snmpget_exit_code1=$?
}

discover_snmp2()
{
_sysname1="`snmpget -O qv ${_snmp_access1} ${_host1} sysName.0 2> /dev/null | grep -Eiv "(No Such|End of)"`"
_snmpget_exit_code2=$?

_sysobjectid1="`snmpget -O qvn ${_snmp_access1} ${_host1} sysObjectID.0 2> /dev/null | grep -Eiv "(No Such|End of)"`"
_snmpget_exit_code3=$?
}

discover_snmp3()
{
if [ ${_snmpget_exit_code1} -eq 0 -a "${_sysdescr1}" != "" ]
then
	discover_snmp2

	echo -e "sysname1=\"${_sysname1}\"\nipv41=\"${_host1}\"\nsysdescr1=\"${_sysdescr1}\"\nsysobjectid1=\"${_sysobjectid1}\"\nsnmp1=\"${_snmp_access1}\"" >> ${_snmp_scan_results1}___${_counter1}

	if [ ! $(( ${_snmpget_exit_code1} + ${_snmpget_exit_code2} + ${_snmpget_exit_code3} )) = 0 ]; then rm ${_snmp_scan_results1}___${_counter1}; fi

	break
fi
}

# EOF
