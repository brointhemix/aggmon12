###
### lib gather1.sh
###

###
### FUNCTIONS
###

derive_nagios_snmp_access_string1()
{
_nagios_snmp_access_string1="`echo ${snmp1} | sed -e 's/-v/-P/' -e 's/-c/-C/' -e 's/-l/-L/' -e 's/-u/-U/'`"
}

###

identify_device1()
{
unset _hostgroups1

if [[ ${sysdescr1} =~ 'IOS XR' ]]
then
        _hostgroups1="cisco-iosxr1"

elif [[ ${sysdescr1} =~ (IOS-XE|LINUX_IOSD) ]]
then
        _hostgroups1="cisco-iosxe1"

elif [[ ${sysdescr1} =~ ' IOS ' ]]
then
        _hostgroups1="cisco-ios1"

elif [[ ${sysdescr1} =~ NX-OS ]]
then
        _hostgroups1="cisco-nxos1"

elif [[ ${sysdescr1} =~ 'Cisco Controller' ]]
then
        _hostgroups1="cisco-aireos1"

elif [[ ${sysdescr1} =~ 'Cisco Wide Area Application Services' ]]
then
        _hostgroups1="cisco-waas1"

elif [[ ${sysdescr1} =~ RouterOS ]]
then
        _hostgroups1="mikrotik-routeros1"

elif [[ ${sysdescr1} =~ 'APC Web' ]]
then
        _hostgroups1="schneider-ups1"

elif [[ ${sysdescr1} =~ (^Linux .*) ]] && [[ ${sysobjectid1} =~ \.1\.3\.6\.1\.4\.1\.17163 ]]
then
        _hostgroups1="riverbed-rios1"

elif [[ ${sysdescr1} =~ (^Linux .*) ]] && [[ ${sysobjectid1} =~ \.1\.3\.6\.1\.4\.1\.8072\.3\.2\.10 ]]
then
        _hostgroups1="linux1"

elif [[ ${sysdescr1} =~ (^FreeBSD .*) ]]
then
        _hostgroups1="freebsd1"

elif [[ ${sysdescr1} =~ (^VMware ESX) ]]
then
        _hostgroups1="vmware-esx1"
else
        _hostgroups1="uncategorised-device1"
fi
}

generate_host1()
{
echo -e \
"\
define host {\n\
 use 1min-check1,host-nograph\n\
 host_name "${sysname2}"\n\
 hostgroups "${_hostgroups1}"\n\
 address "${ipv41}"\n\
}\n\
" > ${__build_dir1}/${sysname2}.cfg
}

generate_host_icmp_check1()
{
echo -e \
"\
define service {\n\
 use 5min-check2,service-graph\n\
 host_name "${sysname2}"\n\
 servicegroups system-performance1,system-rta-pl1\n\
 service_description System Reachability\n\
 check_command check_icmp1!-w 500.0,50% -c 600.0,75%\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_host_sysdescr_check1()
{
echo -e \
"\
define service {\n\
 use 1h-check,service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups system-information1\n\
 service_description System Description\n\
 check_command check_snmp_sysdescr1!${_nagios_snmp_access_string1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_host_uptime_check1()
{
echo -e \
"\
define service {\n\
 use 15min-check,service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups system-performance1\n\
 service_description System Uptime\n\
 check_command check_snmp_uptime1!${_nagios_snmp_access_string1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}
generate_host_uptime_check2()
{
echo -e \
"\
define service {\n\
 use 15min-check,service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups system-performance1\n\
 service_description System Uptime\n\
 check_command check_snmp_uptime2!${_nagios_snmp_access_string1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}
generate_host_uptime_check3()
{
echo -e \
"\
define service {\n\
 use 15min-check,service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups system-performance1\n\
 service_description System Uptime\n\
 check_command check_snmp_uptime3!${_nagios_snmp_access_string1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_interface_traffic1()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 host_name "${sysname2}"\n\
 servicegroups interface-performance1,interface-traffic1\n\
 service_description "${ifDescr[${_index1}]}" Traffic\n\
 check_command check_snmp_iftraffic1!${_nagios_snmp_access_string1} -c $(( ${ifHighSpeed[${_index1}]} * 950000 )),$(( ${ifHighSpeed[${_index1}]} * 950000 ))!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}
generate_interface_traffic2()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 host_name "${sysname2}"\n\
 servicegroups interface-performance1\n\
 service_description "${ifDescr[${_index1}]}" Traffic\n\
 check_command check_snmp_iftraffic1!${_nagios_snmp_access_string1}!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_interface_ifinbroadcastpkts1()
{
echo -e \
"\
define service {\n\
 use service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups interface-performance1,interface-broadcast1\n\
 service_description "${ifDescr[${_index1}]}" Input Broadcast Packets per Second\n\
 check_command check_snmp_ifinbroadcastpkts1!"${_nagios_snmp_access_string1}"!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_interface_ifinerrors1()
{
echo -e \
"\
define service {\n\
 use service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups interface-performance1,interface-error1\n\
 service_description "${ifDescr[${_index1}]}" Input Errors per Second\n\
 check_command check_snmp_ifinerrors1!"${_nagios_snmp_access_string1}"!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_interface_ifoperstatus1()
{
echo -e \
"\
define service {\n\
 use service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups interface-performance1\n\
 service_description "${ifDescr[${_index1}]}" Operating Status\n\
 check_command check_snmp_ifoperstatus1!"${_nagios_snmp_access_string1}"!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_bgp_session_status1()
{
echo -e \
"\
define service {\n\
 use service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups routing-performance1\n\
 service_description BGP Session Status with "${_index1}"\n\
 check_command check_snmp_bgp_peer1!"${_nagios_snmp_access_string1}"!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_eigrp_neighbour_count1()
{
echo -e \
"\
define service {\n\
 use service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups routing-performance1\n\
 service_description EIGRP Neighbour Count\n\
 check_command bkos_check_snmp_cisco_eigrp_neighbour_count.sh!"\"${snmp1}\" ${ipv41} ${_expected_neighbour_count1}"\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_interface_stp_fwd_transitions1()
{
echo -e \
"\
define service {\n\
 use service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups interface-performance1\n\
 service_description "${ifDescr[${_index1}]}" STP Forward Transition Rate\n\
 check_command check_snmp_stp_interface_fwd_transitions1!"${_nagios_snmp_access_string1}"!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_mikrotik_routeros_queue_tree_fwd_bps1()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 host_name "${sysname2}"\n\
 servicegroups qos-performance1\n\
 service_description "${mtxrQueueTreeName[`printf "%d\n" 0x${mtxrQueueTreeParentIndex[${_index1}]}`]:-global}" / "${mtxrQueueTreeName[${_index1}]}" Queue Tree Traffic\n\
 check_command check_snmp_mikrotik_routeros_queue_tree_fwd_bps1!"${_nagios_snmp_access_string1}"!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}
generate_mikrotik_routeros_queue_tree_dropped_pps1()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 host_name "${sysname2}"\n\
 servicegroups qos-performance1\n\
 service_description "${mtxrQueueTreeName[`printf "%d\n" 0x${mtxrQueueTreeParentIndex[${_index1}]}`]:-global}" / "${mtxrQueueTreeName[${_index1}]}" Queue Tree Drops\n\
 check_command check_snmp_mikrotik_routeros_queue_tree_dropped_pps1!"${_nagios_snmp_access_string1}"!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}
generate_mikrotik_routeros_queue_tree_pcq_queues1()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 host_name "${sysname2}"\n\
 servicegroups qos-performance1\n\
 service_description "${mtxrQueueTreeName[`printf "%d\n" 0x${mtxrQueueTreeParentIndex[${_index1}]}`]:-global}" / "${mtxrQueueTreeName[${_index1}]}" PCQ Queue Count\n\
 check_command check_snmp_mikrotik_routeros_queue_tree_pcq_queues1!"${_nagios_snmp_access_string1}"!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_mikrotik_routeros_wlan_physical_ap1()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 host_name "${sysname2}"\n\
 servicegroups wireless-ap-performance1\n\
 service_description Wireless AP ["${mtxrWlApSsid[${_index1}]}"] on "${ifDescr[${_index1}]}" Statistics\n\
 check_command check_snmp_mikrotik_routeros_wlan_physical_ap1!"${_nagios_snmp_access_string1}"!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}
generate_mikrotik_routeros_wlan_virtual_ap1()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 host_name "${sysname2}"\n\
 servicegroups wireless-ap-performance1\n\
 service_description Wireless AP ["${mtxrWlApSsid[${_index1}]}"] on "${ifDescr[${_index1}]}" Statistics\n\
 check_command check_snmp_mikrotik_routeros_wlan_virtual_ap1!"${_nagios_snmp_access_string1}"!${_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_cisco_waas_cwoTfoStatsLoadStatus1()
{
echo -e \
"\
define service {\n\
 use service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups wan-acceleration-status1\n\
 service_description Cisco WAAS Alarm Status\n\
 check_command check_snmp_cisco_waas_cwoTfoStatsLoadStatus1!"${_nagios_snmp_access_string1}"\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_cisco_waas_cwoAoStatsBwOpt1()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 host_name "${sysname2}"\n\
 servicegroups wan-acceleration-performance1\n\
 service_description "${_index1^^}" Acceleration Module Efficiency\n\
 check_command check_snmp_cisco_waas_cwoAoStatsBwOpt1!"${_nagios_snmp_access_string1}"!"${_module_name_char_count1}""${_waas_module_oid1}"\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_cisco_wlc_lap_presence1()
{
echo -e \
"\
define service {\n\
 use service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups wireless-ap-status1\n\
 service_description Wireless AP [${cLApName[${_index1}]}] Presence\n\
 check_command check_snmp_cisco_wlc_lap_presence1!"${_nagios_snmp_access_string1}"!${_ap_index1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}__ap__${_index1//:/_}.cfg
}

generate_cisco_wlc_lap_clsSysApConnectCount1()
{
echo -e \
"\
define service {\n\
 use service-nograph\n\
 host_name "${sysname2}"\n\
 servicegroups wireless-controller-status1\n\
 service_description Wireless Controller AP Count\n\
 check_command check_snmp_cisco_wlc_clsSysApConnectCount1!"${_nagios_snmp_access_string1}"!${_expected_ap_count1}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_cisco_cbqos_bps1()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 max_check_attempts 2\n\
 host_name "${sysname2}"\n\
 servicegroups qos-performance1,cisco-cbqos1\n\
 service_description ${ifDescr[${cbQosIfIndex[${_index1%%.*}]}]} ${cbQosPolicyDirection[${_index1%%.*}]} ${_policy_map1} : ${_class_map1} CBQoS Class Bits per Second\n\
 check_command check_snmp_cisco_cbqos_bps1!"${_nagios_snmp_access_string1}"!${_index1}!"${_alarm_threshold_bps1}"\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}
generate_cisco_cbqos_pps1()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 max_check_attempts 2\n\
 host_name "${sysname2}"\n\
 servicegroups qos-performance1,cisco-cbqos1\n\
 service_description ${ifDescr[${cbQosIfIndex[${_index1%%.*}]}]} ${cbQosPolicyDirection[${_index1%%.*}]} ${_policy_map1} : ${_class_map1} CBQoS Class Packets per Second\n\
 check_command check_snmp_cisco_cbqos_pps1!"${_nagios_snmp_access_string1}"!${_index1}!"${_alarm_threshold_pps1}"\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

generate_cisco_ipsla_sensor1()
{
echo -e \
"\
define service {\n\
 use service-graph\n\
 host_name "${sysname2}"\n\
 servicegroups system-performance1,system-rta-pl1\n\
 service_description IP SLA Sensor for "${_ipsla_monitored_device_ip1#*.}"\n\
 check_command check_snmp_cisco_ipsla_sensor1!"${_nagios_snmp_access_string1}"!${_index1}!${rttMonCtrlAdminThreshold[${_index1}]%% *}\n\
}\n\
" >> ${__build_dir1}/${sysname2}.cfg
}

# EOF
