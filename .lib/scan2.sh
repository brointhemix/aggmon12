###
### lib scan2.sh
###

###
### GENERAL VARIABLES
###

_snmp_mib_scan_results1="${__build_dir1}/snmp_mib_scan_results1"

###
### FUNCTIONS
###

walk_or_bulkwalk1()
{
if [[ ${snmp1} =~ (-v1|-v 1) ]]; then _snmpwalk1="snmpwalk"; else _snmpwalk1="snmpbulkwalk"; fi
}

gather_snmp_data1()
{
echo "declare -A ${_oid1}" >> ${_snmp_mib_scan_results1}___${_counter1}
${_snmpwalk1} -C c -O EXtsq ${snmp1} ${ipv41} ${_oid1} | grep -Ev "(No Such|No more|End of MIB|Unknown Object Identifier)" | sed -e 's/\\"/\"/g' | sed -e 's/\"//g' -e 's/ /=\"/' -e 's/\($\|\s\+$\)/\"/' | sed -r -e 's/\]\[/\./g' -e 's/([a-zA-Z]{2})\.(.*)=/\1\[\2\]=/' -e 's/([a-zA-Z][0-9])\.(.*)=/\1\[\2\]=/' 2> /dev/null >> ${_snmp_mib_scan_results1}___${_counter1}
_snmpwalk_exit_code1=$?
}

# EOF
