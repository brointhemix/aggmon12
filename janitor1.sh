set -x

###
### PRE-RUN
###

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

__basedir1="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
__var0="$0" && __var0="${__var0##*/}"

source ${__basedir1}/.lib0.sh

check_if_already_running1

determine_max_tasks1

load_configuration_file1 ${__var0}.example
load_configuration_file1

###
### LIBRARIES
###

load_library_file1

###
### LOOPS
###

# do_cleanup1

for _index1 in ${ready_dir1[*]}
do
        find -P ${_index1} -mtime ${ready_dir_retention_mtime1}
done

for _index1 in ${hosts_dynamic_dir1[*]}
do
	find -P ${_index1} -mtime ${hosts_dynamic_retention_mtime1}
done

for _index1 in ${pnp4nagios_rrd_dir1[*]}
do
        find -P ${_index1} -mtime ${pnp4nagios_rrd_retention_mtime1}
done

for _index1 in ${check_snmp_dir1[*]}
do
        find -P ${_index1} -mtime ${check_snmp_retention_mtime1}
done

# EOF
